## Writeup 

**Advanced Lane Finding Project**

The goals / steps of this project are the following:

* Compute the camera calibration matrix and distortion coefficients given a set of chessboard images.
* Apply a distortion correction to raw images.
* Use color transforms, gradients, etc., to create a thresholded binary image.
* Apply a perspective transform to rectify binary image ("birds-eye view").
* Detect lane pixels and fit to find the lane boundary.
* Determine the curvature of the lane and vehicle position with respect to center.
* Warp the detected lane boundaries back onto the original image.
* Output visual display of the lane boundaries and numerical estimation of lane curvature and vehicle position.

[//]: # (Image References)

[image1]: ./output_images/calibration_undistorted.jpg
[image2]: ./output_images/test_image.jpg
[image3]: ./output_images/test_binary.jpg
[image4]: ./output_images/warped_straight.jpg
[image5]: ./output_images/output_image.jpg
[image6]: ./output_images/output_lanes.jpg
[video1]: ./project_video.mp4 "Video"

## Rubric Points

### Here I will consider the rubric points individually and describe how I addressed each point in my implementation.

### Writeup / README

#### 1. Provide a Writeup / README that includes all the rubric points and how you addressed each one.  You can submit your writeup as markdown or pdf.  [Here](./writeup_template.md) is a template writeup for this project you can use as a guide and a starting point.  

You're reading it!

### Camera Calibration

#### 1. Briefly state how you computed the camera matrix and distortion coefficients. Provide an example of a distortion corrected calibration image.

The code for this step is contained in the code cells 1-3 of the jupyter notebook provided [here](AdvancedLaneFinding.ipynb).

I start by preparing object points, which will be the (x, y, z) coordinates of the chessboard corners in the world. Here I am assuming the chessboard is fixed on the (x, y) plane at z=0, such that the object points are the same for each calibration image.  Thus, `objp` is just a replicated array of coordinates, and `objpoints` will be appended with a copy of it every time I successfully detect all chessboard corners in a test image.  `imgpoints` will be appended with the (x, y) pixel position of each of the corners in the image plane with each successful chessboard detection.

I then used the output `objpoints` and `imgpoints` to compute the camera calibration and distortion coefficients using the `cv2.calibrateCamera()` function.  I applied this distortion correction to the test image using the `cv2.undistort()` function and obtained this result:

![alt text](./output_images/calibration_original.jpg)

![alt text][image1]

### Pipeline (single images)

#### 1. Provide an example of a distortion-corrected image.

To demonstrate this step, I will describe how I apply the distortion correction to the following test image. Generally I use this image as a running example.

![alt text][image2]

Again, I use the `cv2.undistort()` function and obtain this result:

![alt text](./output_images/undistorted_image.jpg)

#### 2. Describe how (and identify where in your code) you used color transforms, gradients or other methods to create a thresholded binary image.  Provide an example of a binary image result.

I used a combination of color (HLS) and gradient thresholds to generate a binary image (thresholding steps in code cell 5 in function `pipeline()` of the provided jupyter notebook).  Here are the threshholding steps of the running example:

![alt text](./output_images/test_threshold_sobel.jpg)

![alt text](./output_images/test_threshold_color.jpg)

![alt text][image3]

#### 3. Describe how (and identify where in your code) you performed a perspective transform and provide an example of a transformed image.

The code for my perspective transform includes a function called `perspectiv_transform()`, which appears in code cell 10  of the jupyter notebook.  This function takes as input an image and defines source and destination points.  For the source and destination points I picked up four points in a trapezoidal shape that would represent a rectangle when looking down on the road from above. "The easiest way to do this is to investigate an image where the lane lines are straight, and find four points lying along the lines that, after perspective transform, make the lines look straight and vertical from a bird's eye view perspective" (from lecture notes).
I chose following image:

![alt text](./output_images/straight_lines2.jpg)

I obtained following source and destination points::

| Source        | Destination   |
|:-------------:|:-------------:|
| 595, 450      | 200, 0        |
| 240, 720      | 200, 720      |
| 1120, 720     | 960, 720      |
| 690, 450      | 960, 0        |

The perspective transformation was now applied to the image with the straight lane lines and its binary output of the pipeline mentioned above. Source and destination corners were chosen correctly because left and right lane line in the warped image and in the binary warped image are straight, vertical and parallel.

![alt text][image4]

![alt text](./output_images/binary_warped_straight.jpg)

Furthermore the chosen perspective transformation was applied to the binary image of the running example.

![alt text](./output_images/binary_warped_image.jpg)

As expected left and right lane line are (more or less) parallel. Furthermore it is a left curve likewise in the running example.

#### 4. Describe how you identified lane-line pixels and fit their positions with a polynomial?

I identified lane pixels in code cell 17 in the method `createOutput()`. First of all I took a histogram along all the columns in the lower half of the image like. The following chart shows the histogram of the running example:

![alt text](./output_images/histogram.jpg)

The two most prominent peaks in this histogram are good indicators of the x-position of the base of the left and right lane line. So I use these two peaks as a starting point for the left and right lane line, respectivly. With a sliding window technique I follow up each of the two lane lines from the bottom to the top of the image.
At the end I fit the lane lines with a 2nd order polynomial like in the next image that shows the lane lines of the running example.

![alt text][image5]

#### 5. Describe how (and identify where in your code) you calculated the radius of curvature of the lane and the position of the vehicle with respect to center.

I did this in the method `curvature()` in code cell 19 of the jupyter notebook. For the radius I assumed that the curve is a circle (see details [here]("https://www.intmath.com/applications-differentiation/8-radius-curvature.php")). For the deviation of the car from the middle of the road I assumed that the camera is fixed at the center of the car. In the computation I had to fit new polynomials, now in world space instead in pixel space.

#### 6. Provide an example image of your result plotted back down onto the road such that the lane area is identified clearly.

I implemented this step in code cell 21in the function `draw_lane()`.  Here is the result drawn on the running example:

![alt text][image6]

---

### Pipeline (video)

#### 1. Provide a link to your final video output.  Your pipeline should perform reasonably well on the entire project video (wobbly lines are ok but no catastrophic failures that would cause the car to drive off the road!).
![Advanced Lane Finding](out_test_video.mp4)

---

### Discussion

#### 1. Briefly discuss any problems / issues you faced in your implementation of this project.  Where will your pipeline likely fail?  What could you do to make it more robust?

The computer vision pipeline could be improved yet. If shadow becomes darker the pipeline probably fails. Perhaps other color spaces than RGB and HLS could help.
A sanity check of the resulting lane lines could improve the results. Such a sanity check could involve a check of slope and intercepts of the lines whether they are in a reasonable range.
If in the previous frame the lane pixels were detected you can skip the histogram and sliding window step. Instead you can search in a margin around the line position in the previous frame. This would be faster and more robust.
